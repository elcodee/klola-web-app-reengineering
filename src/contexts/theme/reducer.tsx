export const reducer = (state: any, objParams: any) => {
    return {
      ...state,
      theme: objParams.theme ?? state.theme
    };
  };
  
  export const initialState = {
    theme: 'light',
  };
  