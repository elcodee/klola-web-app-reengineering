"use client";

import React from "react";
import {initialState, reducer} from "./reducer";

export const ThemeContext: any = React.createContext({
  state: initialState,
  dispatch: () => null,
});

export const ThemeProvider = ({ children }: any) => {
  const [state, dispatch] = React.useReducer(reducer, initialState);

  return (
    <ThemeContext.Provider value={[state, dispatch]}>
      {children}
    </ThemeContext.Provider>
  );
};
