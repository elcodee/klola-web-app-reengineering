export const reducer = (state: any, objParams: any) => {
    return {
      ...state,
      isAuthenticated: objParams.isAuthenticated ?? state.isAuthenticated
    };
  };
  
  export const initialState = {
    isAuthenticated: false,
  };
  