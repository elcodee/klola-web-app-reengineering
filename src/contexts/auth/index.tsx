"use client";

import React from "react";
import {initialState, reducer} from "./reducer";

export const AuthContext: any = React.createContext({
  state: initialState,
  dispatch: () => null,
});

export const AuthProvider = ({ children }: any) => {
  const [state, dispatch] = React.useReducer(reducer, initialState);

  return (
    <AuthContext.Provider value={[state, dispatch]}>
      {children}
    </AuthContext.Provider>
  );
};
