import DashboardHR from "#/modules/dashboard/hr_dashboard"
import { Col, Row } from "antd"
import Sidebar from "#/components/layouts/sidebar"

export default function Dashboard() {
  return (
    <Sidebar>
      <main className="flex min-h-screen flex-col items-start justify-between p-5">
        <Row justify="space-between" align="middle">
          <Col>
            <DashboardHR />
          </Col>
        </Row>
      </main>
    </Sidebar>
  )
}
