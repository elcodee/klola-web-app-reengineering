import { PiAirplay, PiCalendar, PiWalletBold, PiUsersThreeDuotone, PiCalculator, PiSpeakerNoneDuotone, PiGearFine, PiMegaphone } from "react-icons/pi";

export const mockMenu = [
    {
        "id": 13,
        "name": "Dashboard",
        "icon": PiAirplay,
        "target": "internal",
        "menu_type": "dashboard",
        "link": null,
        "lang": "dashboard",
        "ordering": 1,
        "is_restrict": false,
        "data_active": true,
        "child": {
            "data": []
        }
    },
    {
        "id": 14,
        "name": "Time Management",
        "icon": PiCalendar,
        "target": "internal",
        "menu_type": "ess",
        "link": null,
        "lang": "time",
        "ordering": 2,
        "is_restrict": false,
        "data_active": true,
        "child": {
            "data": [
                {
                    "id": 14,
                    "name": "Leave",
                    "icon": null,
                    "target": "internal",
                    "menu_type": "ess",
                    "link": null,
                    "lang": "leave",
                    "ordering": 1,
                    "is_restrict": false,
                    "data_active": true,
                    "child": {
                        "data": [
                            {
                                "id": 15,
                                "name": "Request",
                                "icon": null,
                                "target": "internal",
                                "menu_type": "ess",
                                "link": "leave",
                                "lang": "request",
                                "ordering": 1,
                                "is_restrict": false,
                                "data_active": true,
                                "child": {
                                    "data": []
                                }
                            },
                            {
                                "id": 16,
                                "name": "Report",
                                "icon": null,
                                "target": "internal",
                                "menu_type": "ess",
                                "link": "leave-report",
                                "lang": "report",
                                "ordering": 2,
                                "is_restrict": false,
                                "data_active": true,
                                "child": {
                                    "data": []
                                }
                            }
                        ]
                    }
                },
                {
                    "id": 17,
                    "name": "Permit",
                    "icon": null,
                    "target": "internal",
                    "menu_type": "ess",
                    "link": null,
                    "lang": "permit",
                    "ordering": 2,
                    "is_restrict": false,
                    "data_active": true,
                    "child": {
                        "data": [
                            {
                                "id": 18,
                                "name": "Request",
                                "icon": null,
                                "target": "internal",
                                "menu_type": "ess",
                                "link": "permit",
                                "lang": "request",
                                "ordering": 1,
                                "is_restrict": false,
                                "data_active": true,
                                "child": {
                                    "data": []
                                }
                            },
                            {
                                "id": 19,
                                "name": "Report",
                                "icon": null,
                                "target": "internal",
                                "menu_type": "ess",
                                "link": "permit-report",
                                "lang": "report",
                                "ordering": 2,
                                "is_restrict": false,
                                "data_active": true,
                                "child": {
                                    "data": []
                                }
                            }
                        ]
                    }
                },
                {
                    "id": 20,
                    "name": "Overtime",
                    "icon": null,
                    "target": "internal",
                    "menu_type": "ess",
                    "link": null,
                    "lang": "overtime",
                    "ordering": 3,
                    "is_restrict": false,
                    "data_active": true,
                    "child": {
                        "data": [
                            {
                                "id": 21,
                                "name": "Request",
                                "icon": null,
                                "target": "internal",
                                "menu_type": "ess",
                                "link": "overtime",
                                "lang": "request",
                                "ordering": 1,
                                "is_restrict": false,
                                "data_active": true,
                                "child": {
                                    "data": []
                                }
                            },
                            {
                                "id": 22,
                                "name": "Report",
                                "icon": null,
                                "target": "internal",
                                "menu_type": "ess",
                                "link": "overtime-report",
                                "lang": "report",
                                "ordering": 2,
                                "is_restrict": false,
                                "data_active": true,
                                "child": {
                                    "data": []
                                }
                            }
                        ]
                    }
                },
                {
                    "id": 23,
                    "name": "Attendance",
                    "icon": null,
                    "target": "internal",
                    "menu_type": "ess",
                    "link": null,
                    "lang": "attendance",
                    "ordering": 4,
                    "is_restrict": false,
                    "data_active": true,
                    "child": {
                        "data": [
                            {
                                "id": 24,
                                "name": "Absence",
                                "icon": null,
                                "target": "internal",
                                "menu_type": "ess",
                                "link": "attendance",
                                "lang": "absence",
                                "ordering": 1,
                                "is_restrict": false,
                                "data_active": true,
                                "child": {
                                    "data": []
                                }
                            },
                            {
                                "id": 25,
                                "name": "Report",
                                "icon": null,
                                "target": "internal",
                                "menu_type": "ess",
                                "link": "attendance-report",
                                "lang": "report",
                                "ordering": 2,
                                "is_restrict": false,
                                "data_active": true,
                                "child": {
                                    "data": []
                                }
                            }
                        ]
                    }
                }
            ]
        }
    },
    {
        "id": 26,
        "name": "Benefits",
        "icon": PiWalletBold,
        "target": "internal",
        "menu_type": "benefit",
        "link": null,
        "lang": "benefits",
        "ordering": 3,
        "is_restrict": false,
        "data_active": true,
        "child": {
            "data": [
                {
                    "id": 27,
                    "name": "Expenses",
                    "icon": null,
                    "target": "internal",
                    "menu_type": "ess",
                    "link": null,
                    "lang": "expenses",
                    "ordering": 1,
                    "is_restrict": false,
                    "data_active": true,
                    "child": {
                        "data": [
                            {
                                "id": 28,
                                "name": "Request",
                                "icon": null,
                                "target": "internal",
                                "menu_type": "ess",
                                "link": "expense",
                                "lang": "request",
                                "ordering": 1,
                                "is_restrict": false,
                                "data_active": true,
                                "child": {
                                    "data": []
                                }
                            },
                            {
                                "id": 29,
                                "name": "Report",
                                "icon": null,
                                "target": "internal",
                                "menu_type": "ess",
                                "link": "expense-report",
                                "lang": "report",
                                "ordering": 2,
                                "is_restrict": false,
                                "data_active": true,
                                "child": {
                                    "data": []
                                }
                            }
                        ]
                    }
                }
            ]
        }
    },
    {
        "id": 39,
        "name": "HR Management",
        "icon": PiUsersThreeDuotone,
        "target": "internal",
        "menu_type": "hris",
        "link": null,
        "lang": "employees",
        "ordering": 4,
        "is_restrict": false,
        "data_active": true,
        "child": {
            "data": [
                {
                    "id": 40,
                    "name": "Employees",
                    "icon": null,
                    "target": "internal",
                    "menu_type": "hris",
                    "link": "employees-hr",
                    "lang": "pr_employee",
                    "ordering": 1,
                    "is_restrict": false,
                    "data_active": true,
                    "child": {
                        "data": []
                    }
                },
                {
                    "id": 206,
                    "name": "Employee Candidate",
                    "icon": null,
                    "target": "internal",
                    "menu_type": "hris",
                    "link": "employees-candidate",
                    "lang": "pr_emp_candidate",
                    "ordering": 2,
                    "is_restrict": false,
                    "data_active": true,
                    "child": {
                        "data": []
                    }
                },
                {
                    "id": 52,
                    "name": "Non Employee",
                    "icon": null,
                    "target": "internal",
                    "menu_type": "payroll_non",
                    "link": "employees-hr-non-employee",
                    "lang": "pr_emp_non",
                    "ordering": 2,
                    "is_restrict": false,
                    "data_active": true,
                    "child": {
                        "data": []
                    }
                },
                {
                    "id": 53,
                    "name": "Employees Bank",
                    "icon": null,
                    "target": "internal",
                    "menu_type": "hris",
                    "link": "employees-bank",
                    "lang": "pr_emp_bank",
                    "ordering": 3,
                    "is_restrict": false,
                    "data_active": true,
                    "child": {
                        "data": []
                    }
                },
                {
                    "id": 54,
                    "name": "Movement",
                    "icon": null,
                    "target": "internal",
                    "menu_type": "hris",
                    "link": "employees-movements",
                    "lang": "movement",
                    "ordering": 4,
                    "is_restrict": false,
                    "data_active": true,
                    "child": {
                        "data": []
                    }
                },
                {
                    "id": 55,
                    "name": "Layoff",
                    "icon": null,
                    "target": "internal",
                    "menu_type": "hris",
                    "link": "employees-layoff",
                    "lang": "layoff",
                    "ordering": 5,
                    "is_restrict": false,
                    "data_active": true,
                    "child": {
                        "data": []
                    }
                },
                {
                    "id": 56,
                    "name": "Reprimand",
                    "icon": null,
                    "target": "internal",
                    "menu_type": "hris",
                    "link": "employees-reprimand",
                    "lang": "reprimand",
                    "ordering": 6,
                    "is_restrict": false,
                    "data_active": true,
                    "child": {
                        "data": []
                    }
                }
            ]
        }
    },
    {
        "id": 58,
        "name": "Payroll Management",
        "icon": PiCalculator,
        "target": "internal",
        "menu_type": "payroll",
        "link": "payroll",
        "lang": "payroll",
        "ordering": 5,
        "is_restrict": false,
        "data_active": true,
        "child": {
            "data": [
                {
                    "id": 59,
                    "name": "Goverment",
                    "icon": null,
                    "target": "internal",
                    "menu_type": "payroll",
                    "link": null,
                    "lang": "pr_gov",
                    "ordering": 1,
                    "is_restrict": false,
                    "data_active": true,
                    "child": {
                        "data": [
                            {
                                "id": 60,
                                "name": "Progressive Tax Rates",
                                "icon": null,
                                "target": "internal",
                                "menu_type": "payroll",
                                "link": "payroll-ms-progressive",
                                "lang": "pr_tr_prog",
                                "ordering": 1,
                                "is_restrict": false,
                                "data_active": true,
                                "child": {
                                    "data": []
                                }
                            },
                            {
                                "id": 62,
                                "name": "PTKP Tax Rates",
                                "icon": null,
                                "target": "internal",
                                "menu_type": "payroll",
                                "link": "payroll-ms-ptkp",
                                "lang": "pr_tr_ptkp",
                                "ordering": 2,
                                "is_restrict": false,
                                "data_active": true,
                                "child": {
                                    "data": []
                                }
                            },
                            {
                                "id": 64,
                                "name": "Position Allowance Rates",
                                "icon": null,
                                "target": "internal",
                                "menu_type": "payroll",
                                "link": "payroll-ms-biayajab",
                                "lang": "pr_tr_jabatan",
                                "ordering": 3,
                                "is_restrict": false,
                                "data_active": true,
                                "child": {
                                    "data": []
                                }
                            },
                            {
                                "id": 65,
                                "name": "Minimum Wage Rates",
                                "icon": null,
                                "target": "internal",
                                "menu_type": "payroll",
                                "link": "payroll-rate-upah-minimum",
                                "lang": "pr_tr_ump",
                                "ordering": 4,
                                "is_restrict": false,
                                "data_active": true,
                                "child": {
                                    "data": []
                                }
                            },
                            {
                                "id": 207,
                                "name": "Tarif Natura",
                                "icon": null,
                                "target": "internal",
                                "menu_type": "a1_payroll",
                                "link": "payroll-rate-natura",
                                "lang": "pr_tr_natura",
                                "ordering": 5,
                                "is_restrict": false,
                                "data_active": true,
                                "child": {
                                    "data": []
                                }
                            },
                            {
                                "id": 212,
                                "name": "Tarif Efektif",
                                "icon": null,
                                "target": "internal",
                                "menu_type": "payroll",
                                "link": "payroll-ms-effective-rate",
                                "lang": "pr_tr_effective_employee",
                                "ordering": 6,
                                "is_restrict": false,
                                "data_active": true,
                                "child": {
                                    "data": []
                                }
                            }
                        ]
                    }
                },
                {
                    "id": 66,
                    "name": "Company",
                    "icon": null,
                    "target": "internal",
                    "menu_type": "payroll",
                    "link": null,
                    "lang": "company",
                    "ordering": 2,
                    "is_restrict": false,
                    "data_active": true,
                    "child": {
                        "data": [
                            {
                                "id": 67,
                                "name": "Payroll Global Setting",
                                "icon": null,
                                "target": "internal",
                                "menu_type": "payroll",
                                "link": "payroll-ms-glb-setting-company",
                                "lang": "pr_global_payroll",
                                "ordering": 1,
                                "is_restrict": false,
                                "data_active": true,
                                "child": {
                                    "data": []
                                }
                            },
                            {
                                "id": 68,
                                "name": "Payroll Group",
                                "icon": null,
                                "target": "internal",
                                "menu_type": "payroll",
                                "link": null,
                                "lang": "pr_group_comp",
                                "ordering": 2,
                                "is_restrict": false,
                                "data_active": true,
                                "child": {
                                    "data": [
                                        {
                                            "id": 69,
                                            "name": "Payroll Group A1",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "a1_payroll",
                                            "link": "payroll-ms-group-company",
                                            "lang": "pr_1721",
                                            "ordering": 1,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": []
                                            }
                                        },
                                        {
                                            "id": 70,
                                            "name": "Payroll Group A2",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "a2_payroll",
                                            "link": "payroll-ms-group-company-a2",
                                            "lang": "pr_1721a2",
                                            "ordering": 2,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": []
                                            }
                                        },
                                        {
                                            "id": 72,
                                            "name": "Payroll Group VII",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "a7_payroll",
                                            "link": "payroll-ms-group-company-vii",
                                            "lang": "pr_emp_vii",
                                            "ordering": 4,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": []
                                            }
                                        }
                                    ]
                                }
                            },
                            {
                                "id": 73,
                                "name": "Payroll Components Setting",
                                "icon": null,
                                "target": "internal",
                                "menu_type": "payroll",
                                "link": null,
                                "lang": "pr_set_comp",
                                "ordering": 3,
                                "is_restrict": false,
                                "data_active": true,
                                "child": {
                                    "data": [
                                        {
                                            "id": 74,
                                            "name": "Setting 1721A1",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "a1_payroll",
                                            "link": "payroll-komponen-setting",
                                            "lang": "pr_1721",
                                            "ordering": 1,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": []
                                            }
                                        },
                                        {
                                            "id": 75,
                                            "name": "Setting 1721A2",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "a2_payroll",
                                            "link": "payroll-komponen-setting-a2",
                                            "lang": "pr_1721a2",
                                            "ordering": 2,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": []
                                            }
                                        },
                                        {
                                            "id": 77,
                                            "name": "Setting 1721-VII",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "a7_payroll",
                                            "link": "payroll-komponen-setting-vii",
                                            "lang": "pr_emp_vii",
                                            "ordering": 4,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": []
                                            }
                                        },
                                        {
                                            "id": 208,
                                            "name": "Setting Natura",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "a1_payroll",
                                            "link": "payroll-komponen-setting-natura",
                                            "lang": "pr_natura",
                                            "ordering": 5,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": []
                                            }
                                        }
                                    ]
                                }
                            },
                            {
                                "id": 78,
                                "name": "Tax Components Setting",
                                "icon": null,
                                "target": "internal",
                                "menu_type": "payroll",
                                "link": null,
                                "lang": "pr_tax_comp",
                                "ordering": 4,
                                "is_restrict": false,
                                "data_active": true,
                                "child": {
                                    "data": [
                                        {
                                            "id": 79,
                                            "name": "Setting 1721A1",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "a1_payroll",
                                            "link": "payroll-tax-setting",
                                            "lang": "pr_1721",
                                            "ordering": 1,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": []
                                            }
                                        },
                                        {
                                            "id": 80,
                                            "name": "Setting 1721A2",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "a2_payroll",
                                            "link": "payroll-tax-setting-a2",
                                            "lang": "pr_1721a2",
                                            "ordering": 2,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": []
                                            }
                                        },
                                        {
                                            "id": 82,
                                            "name": "Setting 1721-VII",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "a7_payroll",
                                            "link": "payroll-tax-setting-vii",
                                            "lang": "pr_emp_vii",
                                            "ordering": 4,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": []
                                            }
                                        }
                                    ]
                                }
                            },
                            {
                                "id": 83,
                                "name": "BPJS TK Rate",
                                "icon": null,
                                "target": "internal",
                                "menu_type": "payroll",
                                "link": "payroll-bpjs-tk",
                                "lang": "pr_tr_bpjs_tk",
                                "ordering": 5,
                                "is_restrict": false,
                                "data_active": true,
                                "child": {
                                    "data": []
                                }
                            },
                            {
                                "id": 84,
                                "name": "BPJS KS Rate",
                                "icon": null,
                                "target": "internal",
                                "menu_type": "payroll",
                                "link": "payroll-bpjs-ks",
                                "lang": "pr_tr_bpjs_ks",
                                "ordering": 6,
                                "is_restrict": false,
                                "data_active": true,
                                "child": {
                                    "data": []
                                }
                            },
                            {
                                "id": 85,
                                "name": "Base Income Setting",
                                "icon": null,
                                "target": "internal",
                                "menu_type": "payroll",
                                "link": "payroll-dasar-upah",
                                "lang": "pr_dasar_upah",
                                "ordering": 7,
                                "is_restrict": false,
                                "data_active": true,
                                "child": {
                                    "data": []
                                }
                            },
                            {
                                "id": 86,
                                "name": "Prorate Setting",
                                "icon": null,
                                "target": "internal",
                                "menu_type": "payroll",
                                "link": "payroll-prorate",
                                "lang": "pr_prorate",
                                "ordering": 8,
                                "is_restrict": false,
                                "data_active": true,
                                "child": {
                                    "data": []
                                }
                            }
                        ]
                    }
                },
                {
                    "id": 88,
                    "name": "Payroll",
                    "icon": null,
                    "target": "internal",
                    "menu_type": "payroll",
                    "link": null,
                    "lang": "pr_payroll",
                    "ordering": 3,
                    "is_restrict": false,
                    "data_active": true,
                    "child": {
                        "data": [
                            {
                                "id": 89,
                                "name": "Regular Employee",
                                "icon": null,
                                "target": "internal",
                                "menu_type": "a1_payroll",
                                "link": null,
                                "lang": "pr_emp_tetap",
                                "ordering": 1,
                                "is_restrict": false,
                                "data_active": true,
                                "child": {
                                    "data": [
                                        {
                                            "id": 90,
                                            "name": "Participation",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "a1_payroll",
                                            "link": "payroll-upload-kepesertaan",
                                            "lang": "pr_peserta",
                                            "ordering": 1,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": []
                                            }
                                        },
                                        {
                                            "id": 91,
                                            "name": "Fixed Pay",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "a1_payroll",
                                            "link": "payroll-tetap",
                                            "lang": "pr_dataprfix",
                                            "ordering": 2,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": []
                                            }
                                        },
                                        {
                                            "id": 92,
                                            "name": "Variable Pay",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "a1_payroll",
                                            "link": "payroll-tidak-tetap",
                                            "lang": "pr_dataprvar",
                                            "ordering": 3,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": []
                                            }
                                        },
                                        {
                                            "id": 93,
                                            "name": "Consolidation",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "a1_payroll",
                                            "link": "payroll-consolidation",
                                            "lang": "pr_consolidation",
                                            "ordering": 4,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": []
                                            }
                                        },
                                        {
                                            "id": 209,
                                            "name": "Upload Natura",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "a1_payroll",
                                            "link": "payroll-upload-natura",
                                            "lang": "pr_natura",
                                            "ordering": 5,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": []
                                            }
                                        }
                                    ]
                                }
                            },
                            {
                                "id": 94,
                                "name": "Civil Servant",
                                "icon": null,
                                "target": "internal",
                                "menu_type": "a2_payroll",
                                "link": null,
                                "lang": "pr_emp_negeri",
                                "ordering": 2,
                                "is_restrict": false,
                                "data_active": true,
                                "child": {
                                    "data": [
                                        {
                                            "id": 96,
                                            "name": "1721A2",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "a2_payroll",
                                            "link": "payroll-a2",
                                            "lang": "pr_datapra2",
                                            "ordering": 2,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": []
                                            }
                                        }
                                    ]
                                }
                            },
                            {
                                "id": 97,
                                "name": "Non Employee",
                                "icon": null,
                                "target": "internal",
                                "menu_type": "payroll_non",
                                "link": null,
                                "lang": "pr_emp_non",
                                "ordering": 3,
                                "is_restrict": false,
                                "data_active": true,
                                "child": {
                                    "data": [
                                        {
                                            "id": 98,
                                            "name": "1721-VI",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "a6_payroll",
                                            "link": "payroll-vi",
                                            "lang": "pr_datapra6",
                                            "ordering": 1,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": []
                                            }
                                        },
                                        {
                                            "id": 99,
                                            "name": "1721-VII",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "a7_payroll",
                                            "link": "payroll-vii",
                                            "lang": "pr_datapra7",
                                            "ordering": 2,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": []
                                            }
                                        }
                                    ]
                                }
                            },
                            {
                                "id": 100,
                                "name": "Pre-Process",
                                "icon": null,
                                "target": "internal",
                                "menu_type": "payroll",
                                "link": null,
                                "lang": "pr_preprocess",
                                "ordering": 4,
                                "is_restrict": false,
                                "data_active": true,
                                "child": {
                                    "data": [
                                        {
                                            "id": 101,
                                            "name": "Pre-Process Pegawai Tetap",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "a1_payroll",
                                            "link": "payroll-pre-process",
                                            "lang": "pr_1721",
                                            "ordering": 1,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": []
                                            }
                                        },
                                        {
                                            "id": 102,
                                            "name": "Pre-Process Pegawai Negeri",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "a2_payroll",
                                            "link": "payroll-a2-process",
                                            "lang": "pr_1721a2",
                                            "ordering": 2,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": []
                                            }
                                        },
                                        {
                                            "id": 103,
                                            "name": "Pre-Process Non Pegawai VI",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "a6_payroll",
                                            "link": "payroll-a6-process",
                                            "lang": "pr_emp_vi",
                                            "ordering": 3,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": []
                                            }
                                        },
                                        {
                                            "id": 104,
                                            "name": "Pre-Process Non Pegawai VII",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "a7_payroll",
                                            "link": "payroll-a7-process",
                                            "lang": "pr_emp_vii",
                                            "ordering": 4,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": []
                                            }
                                        }
                                    ]
                                }
                            }
                        ]
                    }
                },
                {
                    "id": 105,
                    "name": "Reports",
                    "icon": null,
                    "target": "internal",
                    "menu_type": "payroll",
                    "link": null,
                    "lang": "pr_tax_final",
                    "ordering": 4,
                    "is_restrict": false,
                    "data_active": true,
                    "child": {
                        "data": [
                            {
                                "id": 106,
                                "name": "Regular Employee",
                                "icon": null,
                                "target": "internal",
                                "menu_type": "a1_payroll",
                                "link": null,
                                "lang": "pr_emp_tetap",
                                "ordering": 1,
                                "is_restrict": false,
                                "data_active": true,
                                "child": {
                                    "data": [
                                        {
                                            "id": 107,
                                            "name": "Process",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "a1_payroll",
                                            "link": "payroll-pre-process-report",
                                            "lang": "pr_process",
                                            "ordering": 1,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": []
                                            }
                                        },
                                        {
                                            "id": 108,
                                            "name": "Bonus",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "a1_payroll",
                                            "link": "payroll-pre-process-report-tantiem",
                                            "lang": "pr_tantiem",
                                            "ordering": 2,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": []
                                            }
                                        },
                                        {
                                            "id": 109,
                                            "name": "Standard",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "a1_payroll",
                                            "link": "payroll-standard-report",
                                            "lang": "pr_repot",
                                            "ordering": 3,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": []
                                            }
                                        }
                                    ]
                                }
                            },
                            {
                                "id": 110,
                                "name": "Civil Servant",
                                "icon": null,
                                "target": "internal",
                                "menu_type": "a2_payroll",
                                "link": null,
                                "lang": "pr_emp_negeri",
                                "ordering": 2,
                                "is_restrict": false,
                                "data_active": true,
                                "child": {
                                    "data": [
                                        {
                                            "id": 111,
                                            "name": "Process",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "a2_payroll",
                                            "link": "payroll-a2-report",
                                            "lang": "pr_process",
                                            "ordering": 1,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": []
                                            }
                                        },
                                        {
                                            "id": 112,
                                            "name": "Standard",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "a2_payroll",
                                            "link": "payroll-standard-report-a2",
                                            "lang": "pr_repot",
                                            "ordering": 2,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": []
                                            }
                                        }
                                    ]
                                }
                            },
                            {
                                "id": 113,
                                "name": "Non Pegawai",
                                "icon": null,
                                "target": "internal",
                                "menu_type": "payroll_non",
                                "link": null,
                                "lang": "pr_emp_non",
                                "ordering": 3,
                                "is_restrict": false,
                                "data_active": true,
                                "child": {
                                    "data": [
                                        {
                                            "id": 114,
                                            "name": "Not Final",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "a6_payroll",
                                            "link": null,
                                            "lang": "pr_1721vi",
                                            "ordering": 1,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": [
                                                    {
                                                        "id": 115,
                                                        "name": "Process",
                                                        "icon": null,
                                                        "target": "internal",
                                                        "menu_type": "a6_payroll",
                                                        "link": "payroll-a6-report",
                                                        "lang": "pr_process",
                                                        "ordering": 1,
                                                        "is_restrict": false,
                                                        "data_active": true,
                                                        "child": {
                                                            "data": []
                                                        }
                                                    },
                                                    {
                                                        "id": 116,
                                                        "name": "Standard",
                                                        "icon": null,
                                                        "target": "internal",
                                                        "menu_type": "a6_payroll",
                                                        "link": "payroll-standard-report-a6",
                                                        "lang": "pr_repot",
                                                        "ordering": 2,
                                                        "is_restrict": false,
                                                        "data_active": true,
                                                        "child": {
                                                            "data": []
                                                        }
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "id": 117,
                                            "name": "Final",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "a7_payroll",
                                            "link": null,
                                            "lang": "pr_1721vii",
                                            "ordering": 2,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": [
                                                    {
                                                        "id": 118,
                                                        "name": "Process",
                                                        "icon": null,
                                                        "target": "internal",
                                                        "menu_type": "a7_payroll",
                                                        "link": "payroll-a7-report",
                                                        "lang": "pr_process",
                                                        "ordering": 1,
                                                        "is_restrict": false,
                                                        "data_active": true,
                                                        "child": {
                                                            "data": []
                                                        }
                                                    },
                                                    {
                                                        "id": 119,
                                                        "name": "Standard",
                                                        "icon": null,
                                                        "target": "internal",
                                                        "menu_type": "a7_payroll",
                                                        "link": "payroll-standard-report-a7",
                                                        "lang": "pr_repot",
                                                        "ordering": 2,
                                                        "is_restrict": false,
                                                        "data_active": true,
                                                        "child": {
                                                            "data": []
                                                        }
                                                    }
                                                ]
                                            }
                                        }
                                    ]
                                }
                            }
                        ]
                    }
                },
                {
                    "id": 120,
                    "name": "Prints",
                    "icon": null,
                    "target": "internal",
                    "menu_type": "payroll",
                    "link": null,
                    "lang": "print",
                    "ordering": 5,
                    "is_restrict": false,
                    "data_active": true,
                    "child": {
                        "data": [
                            {
                                "id": 121,
                                "name": "Form 1721 A1",
                                "icon": null,
                                "target": "internal",
                                "menu_type": "a1_payroll",
                                "link": "payroll-1721a1-report",
                                "lang": "print_1721",
                                "ordering": 1,
                                "is_restrict": false,
                                "data_active": true,
                                "child": {
                                    "data": []
                                }
                            },
                            {
                                "id": 122,
                                "name": "Form 1721 A2",
                                "icon": null,
                                "target": "internal",
                                "menu_type": "a2_payroll",
                                "link": "payroll-1721a2-report",
                                "lang": "print_1721a2",
                                "ordering": 2,
                                "is_restrict": false,
                                "data_active": true,
                                "child": {
                                    "data": []
                                }
                            },
                            {
                                "id": 123,
                                "name": "Form 1721 VI",
                                "icon": null,
                                "target": "internal",
                                "menu_type": "a6_payroll",
                                "link": "payroll-1721vi-report",
                                "lang": "print_1721vi",
                                "ordering": 3,
                                "is_restrict": false,
                                "data_active": true,
                                "child": {
                                    "data": []
                                }
                            },
                            {
                                "id": 124,
                                "name": "Form 1721 VII",
                                "icon": null,
                                "target": "internal",
                                "menu_type": "a7_payroll",
                                "link": "payroll-1721vii-report",
                                "lang": "print_1721vii",
                                "ordering": 4,
                                "is_restrict": false,
                                "data_active": true,
                                "child": {
                                    "data": []
                                }
                            }
                        ]
                    }
                },
                {
                    "id": 125,
                    "name": "Payroll Simulator",
                    "icon": null,
                    "target": "internal",
                    "menu_type": "a1_payroll",
                    "link": "payroll-simulator",
                    "lang": "calculator",
                    "ordering": 6,
                    "is_restrict": false,
                    "data_active": true,
                    "child": {
                        "data": []
                    }
                }
            ]
        }
    },
    {
        "id": 128,
        "name": "Announcement",
        "icon": PiMegaphone,
        "target": "internal",
        "menu_type": "hris",
        "link": null,
        "lang": "announcement",
        "ordering": 8,
        "is_restrict": false,
        "data_active": true,
        "child": {
            "data": [
                {
                    "id": 129,
                    "name": "Post",
                    "icon": null,
                    "target": "internal",
                    "menu_type": "hris",
                    "link": "announcement",
                    "lang": "post",
                    "ordering": 1,
                    "is_restrict": false,
                    "data_active": true,
                    "child": {
                        "data": []
                    }
                },
                {
                    "id": 130,
                    "name": "Inbox",
                    "icon": null,
                    "target": "internal",
                    "menu_type": "hris",
                    "link": "announcement-inbox",
                    "lang": "inbox",
                    "ordering": 2,
                    "is_restrict": false,
                    "data_active": true,
                    "child": {
                        "data": []
                    }
                }
            ]
        }
    },
    {
        "id": 131,
        "name": "Setting",
        "icon": PiGearFine,
        "target": "internal",
        "menu_type": "hris",
        "link": null,
        "lang": "setting",
        "ordering": 9,
        "is_restrict": false,
        "data_active": true,
        "child": {
            "data": [
                {
                    "id": 132,
                    "name": "Company Setting",
                    "icon": null,
                    "target": "internal",
                    "menu_type": "hris",
                    "link": "settings-system-company",
                    "lang": "company",
                    "ordering": 1,
                    "is_restrict": false,
                    "data_active": true,
                    "child": {
                        "data": []
                    }
                },
                {
                    "id": 134,
                    "name": "User Management",
                    "icon": null,
                    "target": "internal",
                    "menu_type": "hris",
                    "link": "settings-users",
                    "lang": "users",
                    "ordering": 2,
                    "is_restrict": false,
                    "data_active": true,
                    "child": {
                        "data": []
                    }
                },
                {
                    "id": 141,
                    "name": "Payroll",
                    "icon": null,
                    "target": "internal",
                    "menu_type": "payroll",
                    "link": null,
                    "lang": "pr_payroll",
                    "ordering": 4,
                    "is_restrict": false,
                    "data_active": true,
                    "child": {
                        "data": [
                            {
                                "id": 142,
                                "name": "Payslip",
                                "icon": null,
                                "target": "internal",
                                "menu_type": "payroll",
                                "link": "settings-payroll-payslip",
                                "lang": "pr_payslip",
                                "ordering": 1,
                                "is_restrict": false,
                                "data_active": true,
                                "child": {
                                    "data": []
                                }
                            }
                        ]
                    }
                },
                {
                    "id": 143,
                    "name": "Time Management",
                    "icon": null,
                    "target": "internal",
                    "menu_type": "ess",
                    "link": null,
                    "lang": "time",
                    "ordering": 5,
                    "is_restrict": false,
                    "data_active": true,
                    "child": {
                        "data": [
                            {
                                "id": 144,
                                "name": "Leave",
                                "icon": null,
                                "target": "internal",
                                "menu_type": "ess",
                                "link": null,
                                "lang": "leave",
                                "ordering": 1,
                                "is_restrict": false,
                                "data_active": true,
                                "child": {
                                    "data": [
                                        {
                                            "id": 145,
                                            "name": "Quotas",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "ess",
                                            "link": "leave-quotas",
                                            "lang": "quota",
                                            "ordering": 1,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": []
                                            }
                                        },
                                        {
                                            "id": 146,
                                            "name": "Category",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "ess",
                                            "link": "leave-category",
                                            "lang": "category",
                                            "ordering": 2,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": []
                                            }
                                        }
                                    ]
                                }
                            },
                            {
                                "id": 147,
                                "name": "Permit",
                                "icon": null,
                                "target": "internal",
                                "menu_type": "ess",
                                "link": null,
                                "lang": "permit",
                                "ordering": 2,
                                "is_restrict": false,
                                "data_active": true,
                                "child": {
                                    "data": [
                                        {
                                            "id": 148,
                                            "name": "Category",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "ess",
                                            "link": "permit-category",
                                            "lang": "category",
                                            "ordering": 1,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": []
                                            }
                                        }
                                    ]
                                }
                            },
                            {
                                "id": 149,
                                "name": "Overtime",
                                "icon": null,
                                "target": "internal",
                                "menu_type": "payroll",
                                "link": null,
                                "lang": "overtime",
                                "ordering": 3,
                                "is_restrict": false,
                                "data_active": true,
                                "child": {
                                    "data": [
                                        {
                                            "id": 150,
                                            "name": "Overtime Allowance",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "payroll",
                                            "link": "overtime-setting-allowence",
                                            "lang": "allowance",
                                            "ordering": 1,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": []
                                            }
                                        }
                                    ]
                                }
                            },
                            {
                                "id": 151,
                                "name": "Attendance",
                                "icon": null,
                                "target": "internal",
                                "menu_type": "ess",
                                "link": null,
                                "lang": "attendance",
                                "ordering": 4,
                                "is_restrict": false,
                                "data_active": true,
                                "child": {
                                    "data": [
                                        {
                                            "id": 205,
                                            "name": "Attendance Allowance",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "payroll",
                                            "link": "attendance-setting-allowence",
                                            "lang": "allowance",
                                            "ordering": 1,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": []
                                            }
                                        }
                                    ]
                                }
                            },
                            {
                                "id": 152,
                                "name": "Scheduling",
                                "icon": null,
                                "target": "internal",
                                "menu_type": "ess",
                                "link": "scheduling",
                                "lang": "scheduling",
                                "ordering": 5,
                                "is_restrict": false,
                                "data_active": true,
                                "child": {
                                    "data": [
                                        {
                                            "id": 153,
                                            "name": "Work Shift",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "ess",
                                            "link": "scheduling-shift",
                                            "lang": "shift",
                                            "ordering": 1,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": []
                                            }
                                        },
                                        {
                                            "id": 154,
                                            "name": "Holidays",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "ess",
                                            "link": "scheduling-holidays",
                                            "lang": "holidays",
                                            "ordering": 2,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": []
                                            }
                                        }
                                    ]
                                }
                            }
                        ]
                    }
                },
                {
                    "id": 155,
                    "name": "Benefits",
                    "icon": null,
                    "target": "internal",
                    "menu_type": "ess",
                    "link": null,
                    "lang": "benefits",
                    "ordering": 6,
                    "is_restrict": false,
                    "data_active": true,
                    "child": {
                        "data": [
                            {
                                "id": 156,
                                "name": "Expenses",
                                "icon": null,
                                "target": "internal",
                                "menu_type": "ess",
                                "link": null,
                                "lang": "expenses",
                                "ordering": 1,
                                "is_restrict": false,
                                "data_active": true,
                                "child": {
                                    "data": [
                                        {
                                            "id": 157,
                                            "name": "Category",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "ess",
                                            "link": "expense-category",
                                            "lang": "category",
                                            "ordering": 1,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": []
                                            }
                                        }
                                    ]
                                }
                            }
                        ]
                    }
                },
                {
                    "id": 168,
                    "name": "Master Data",
                    "icon": null,
                    "target": "internal",
                    "menu_type": "hris",
                    "link": null,
                    "lang": "master",
                    "ordering": 7,
                    "is_restrict": false,
                    "data_active": true,
                    "child": {
                        "data": [
                            {
                                "id": 169,
                                "name": "General",
                                "icon": null,
                                "target": "internal",
                                "menu_type": "hris",
                                "link": null,
                                "lang": "general",
                                "ordering": 1,
                                "is_restrict": false,
                                "data_active": true,
                                "child": {
                                    "data": [
                                        {
                                            "id": 170,
                                            "name": "Education",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "hris",
                                            "link": null,
                                            "lang": "education",
                                            "ordering": 1,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": [
                                                    {
                                                        "id": 171,
                                                        "name": "Formal",
                                                        "icon": null,
                                                        "target": "internal",
                                                        "menu_type": "hris",
                                                        "link": "master-edu-formal",
                                                        "lang": "formal",
                                                        "ordering": 1,
                                                        "is_restrict": false,
                                                        "data_active": true,
                                                        "child": {
                                                            "data": []
                                                        }
                                                    },
                                                    {
                                                        "id": 172,
                                                        "name": "Non Formal",
                                                        "icon": null,
                                                        "target": "internal",
                                                        "menu_type": "hris",
                                                        "link": "master-edu-nonformal",
                                                        "lang": "nonformal",
                                                        "ordering": 2,
                                                        "is_restrict": false,
                                                        "data_active": true,
                                                        "child": {
                                                            "data": []
                                                        }
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "id": 173,
                                            "name": "Administrative Area",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "hris",
                                            "link": null,
                                            "lang": "administrative",
                                            "ordering": 2,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": [
                                                    {
                                                        "id": 174,
                                                        "name": "Province",
                                                        "icon": null,
                                                        "target": "internal",
                                                        "menu_type": "hris",
                                                        "link": "master-province",
                                                        "lang": "province",
                                                        "ordering": 1,
                                                        "is_restrict": false,
                                                        "data_active": true,
                                                        "child": {
                                                            "data": []
                                                        }
                                                    },
                                                    {
                                                        "id": 175,
                                                        "name": "Regency",
                                                        "icon": null,
                                                        "target": "internal",
                                                        "menu_type": "hris",
                                                        "link": "master-regency",
                                                        "lang": "regency",
                                                        "ordering": 2,
                                                        "is_restrict": false,
                                                        "data_active": true,
                                                        "child": {
                                                            "data": []
                                                        }
                                                    },
                                                    {
                                                        "id": 176,
                                                        "name": "Districts",
                                                        "icon": null,
                                                        "target": "internal",
                                                        "menu_type": "hris",
                                                        "link": "master-subdistrict",
                                                        "lang": "district",
                                                        "ordering": 3,
                                                        "is_restrict": false,
                                                        "data_active": true,
                                                        "child": {
                                                            "data": []
                                                        }
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "id": 177,
                                            "name": "Regional Tax Offices",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "hris",
                                            "link": "master-tax-offices",
                                            "lang": "tax_offices",
                                            "ordering": 3,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": []
                                            }
                                        },
                                        {
                                            "id": 179,
                                            "name": "Digital Documents",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "hris",
                                            "link": "master-digidoc",
                                            "lang": "digidoc",
                                            "ordering": 5,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": []
                                            }
                                        },
                                        {
                                            "id": 180,
                                            "name": "Bank",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "hris",
                                            "link": "master-bank",
                                            "lang": "bank",
                                            "ordering": 6,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": []
                                            }
                                        },
                                        {
                                            "id": 181,
                                            "name": "Personal",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "hris",
                                            "link": null,
                                            "lang": "personal",
                                            "ordering": 7,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": [
                                                    {
                                                        "id": 182,
                                                        "name": "Type of ID",
                                                        "icon": null,
                                                        "target": "internal",
                                                        "menu_type": "hris",
                                                        "link": "master-idcard",
                                                        "lang": "id_type",
                                                        "ordering": 1,
                                                        "is_restrict": false,
                                                        "data_active": true,
                                                        "child": {
                                                            "data": []
                                                        }
                                                    },
                                                    {
                                                        "id": 183,
                                                        "name": "Religion",
                                                        "icon": null,
                                                        "target": "internal",
                                                        "menu_type": "hris",
                                                        "link": "master-religion",
                                                        "lang": "religion",
                                                        "ordering": 2,
                                                        "is_restrict": false,
                                                        "data_active": true,
                                                        "child": {
                                                            "data": []
                                                        }
                                                    },
                                                    {
                                                        "id": 184,
                                                        "name": "Marital Status",
                                                        "icon": null,
                                                        "target": "internal",
                                                        "menu_type": "hris",
                                                        "link": "master-marriage",
                                                        "lang": "marital",
                                                        "ordering": 3,
                                                        "is_restrict": false,
                                                        "data_active": true,
                                                        "child": {
                                                            "data": []
                                                        }
                                                    },
                                                    {
                                                        "id": 185,
                                                        "name": "Consanguinity",
                                                        "icon": null,
                                                        "target": "internal",
                                                        "menu_type": "hris",
                                                        "link": "master-consanguinity",
                                                        "lang": "consanguinity",
                                                        "ordering": 4,
                                                        "is_restrict": false,
                                                        "data_active": true,
                                                        "child": {
                                                            "data": []
                                                        }
                                                    }
                                                ]
                                            }
                                        }
                                    ]
                                }
                            },
                            {
                                "id": 186,
                                "name": "Organization",
                                "icon": null,
                                "target": "internal",
                                "menu_type": "hris",
                                "link": null,
                                "lang": "organization",
                                "ordering": 2,
                                "is_restrict": false,
                                "data_active": true,
                                "child": {
                                    "data": [
                                        {
                                            "id": 187,
                                            "name": "Structure",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "hris",
                                            "link": "master-structure",
                                            "lang": "structure",
                                            "ordering": 1,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": []
                                            }
                                        },
                                        {
                                            "id": 188,
                                            "name": "Label",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "hris",
                                            "link": null,
                                            "lang": "label",
                                            "ordering": 2,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": [
                                                    {
                                                        "id": 189,
                                                        "name": "Departments",
                                                        "icon": null,
                                                        "target": "internal",
                                                        "menu_type": "hris",
                                                        "link": "master-company-departments",
                                                        "lang": "departments",
                                                        "ordering": 1,
                                                        "is_restrict": false,
                                                        "data_active": true,
                                                        "child": {
                                                            "data": []
                                                        }
                                                    },
                                                    {
                                                        "id": 191,
                                                        "name": "Position",
                                                        "icon": null,
                                                        "target": "internal",
                                                        "menu_type": "hris",
                                                        "link": "master-position",
                                                        "lang": "position",
                                                        "ordering": 2,
                                                        "is_restrict": false,
                                                        "data_active": true,
                                                        "child": {
                                                            "data": []
                                                        }
                                                    },
                                                    {
                                                        "id": 192,
                                                        "name": "Grade",
                                                        "icon": null,
                                                        "target": "internal",
                                                        "menu_type": "hris",
                                                        "link": "master-grade",
                                                        "lang": "grade",
                                                        "ordering": 3,
                                                        "is_restrict": false,
                                                        "data_active": true,
                                                        "child": {
                                                            "data": []
                                                        }
                                                    },
                                                    {
                                                        "id": 193,
                                                        "name": "Rank",
                                                        "icon": null,
                                                        "target": "internal",
                                                        "menu_type": "hris",
                                                        "link": "master-rank",
                                                        "lang": "rank",
                                                        "ordering": 4,
                                                        "is_restrict": false,
                                                        "data_active": true,
                                                        "child": {
                                                            "data": []
                                                        }
                                                    },
                                                    {
                                                        "id": 194,
                                                        "name": "Unit",
                                                        "icon": null,
                                                        "target": "internal",
                                                        "menu_type": "hris",
                                                        "link": "master-unit",
                                                        "lang": "unit",
                                                        "ordering": 5,
                                                        "is_restrict": false,
                                                        "data_active": true,
                                                        "child": {
                                                            "data": []
                                                        }
                                                    },
                                                    {
                                                        "id": 195,
                                                        "name": "Divisi",
                                                        "icon": null,
                                                        "target": "internal",
                                                        "menu_type": "hris",
                                                        "link": "master-divisi",
                                                        "lang": "divisi",
                                                        "ordering": 6,
                                                        "is_restrict": false,
                                                        "data_active": true,
                                                        "child": {
                                                            "data": []
                                                        }
                                                    },
                                                    {
                                                        "id": 196,
                                                        "name": "Employee Groups",
                                                        "icon": null,
                                                        "target": "internal",
                                                        "menu_type": "hris",
                                                        "link": "master-group-pegawai",
                                                        "lang": "pr_emp_group",
                                                        "ordering": 7,
                                                        "is_restrict": false,
                                                        "data_active": true,
                                                        "child": {
                                                            "data": []
                                                        }
                                                    }
                                                ]
                                            }
                                        }
                                    ]
                                }
                            },
                            {
                                "id": 197,
                                "name": "HR Administration",
                                "icon": null,
                                "target": "internal",
                                "menu_type": "hris",
                                "link": null,
                                "lang": "adm_sdm",
                                "ordering": 3,
                                "is_restrict": false,
                                "data_active": true,
                                "child": {
                                    "data": [
                                        {
                                            "id": 198,
                                            "name": "Employee Status",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "hris",
                                            "link": "master-emp-status",
                                            "lang": "empstatus",
                                            "ordering": 1,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": []
                                            }
                                        },
                                        {
                                            "id": 199,
                                            "name": "Layoff",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "hris",
                                            "link": "master-layoff",
                                            "lang": "adm_phk",
                                            "ordering": 2,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": []
                                            }
                                        },
                                        {
                                            "id": 200,
                                            "name": "Reprimand",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "hris",
                                            "link": "master-reprimand",
                                            "lang": "adm_causecode",
                                            "ordering": 3,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": []
                                            }
                                        },
                                        {
                                            "id": 211,
                                            "name": "Employee Tax Status",
                                            "icon": null,
                                            "target": "internal",
                                            "menu_type": "hris",
                                            "link": "master-emp-tax-status",
                                            "lang": "emp_tax_status",
                                            "ordering": 4,
                                            "is_restrict": false,
                                            "data_active": true,
                                            "child": {
                                                "data": []
                                            }
                                        }
                                    ]
                                }
                            }
                        ]
                    }
                },
                {
                    "id": 204,
                    "name": "Activity Logs",
                    "icon": null,
                    "target": "internal",
                    "menu_type": "hris",
                    "link": "settings-activity",
                    "lang": "activity",
                    "ordering": 8,
                    "is_restrict": false,
                    "data_active": true,
                    "child": {
                        "data": []
                    }
                }
            ]
        }
    }
]