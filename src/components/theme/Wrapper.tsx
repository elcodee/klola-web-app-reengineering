"use client";

import React, { useContext } from "react";
import { ConfigProvider, theme } from "antd";
import { ThemeContext } from "#/contexts/theme";

export default function Wrapper(props: any) {
  const { children } = props;
  const [state]: any = useContext(ThemeContext);

  return (
    <ConfigProvider
      theme={{
        algorithm: state?.theme === "dark" ? theme.darkAlgorithm : theme.defaultAlgorithm,
      }}
    >
      {children}
    </ConfigProvider>
  );
}
