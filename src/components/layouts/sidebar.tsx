"use client";

import React, { useContext, useState } from 'react';
import type { MenuProps, MenuTheme } from 'antd';
import { Breadcrumb, Button, Col, Image, Layout, Menu, Row, Switch, theme } from 'antd';
import { mockMenu } from '#/mocks/menu';
import UserProfile from './UserProfile';
import { PiArrowLineLeftDuotone, PiArrowLineRightDuotone, PiMoonStars, PiSun, PiToggleLeft, PiToggleRight } from 'react-icons/pi';
import { MenuFoldOutlined, MenuUnfoldOutlined } from '@ant-design/icons';
import { ThemeContext } from '#/contexts/theme';

const { Header, Content, Sider } = Layout;

const items1: MenuProps['items'] = ['1'].map((key) => ({
    key,
    label: `nav ${key}`,
}));


// const items2: MenuProps['items'] = [UserOutlined, LaptopOutlined, NotificationOutlined].map(
const items2: MenuProps['items'] = mockMenu.map(item => {
    if (item.child?.data?.length > 0) {
        return {
            key: item.id,
            icon: React.createElement(item.icon),
            label: item.name,
            children: item.child?.data.map((submenu1: any, key1: number) => {
                if (submenu1.child?.data?.length > 0) {
                    return {
                        key: submenu1.id,
                        icon: submenu1.icon ? React.createElement(submenu1.icon) : null,
                        label: submenu1.name,
                        children: submenu1.child?.data.map((submenu2: any, key2: number) => {
                            if (submenu2.child?.data?.length > 0) {
                                return {
                                    key: submenu2.id,
                                    icon: submenu2.icon ? React.createElement(submenu2.icon) : null,
                                    label: submenu2.name,
                                    children: submenu2.child?.data.map((submenu3: any, key2: number) => {
                                        if (submenu3.child?.data?.length > 0) {
                                            return {
                                                key: submenu3.id,
                                                icon: submenu3.icon ? React.createElement(submenu3.icon) : null,
                                                label: submenu3.name,
                                                children: submenu3.child?.data.map((submenu4: any, key2: number) => {
                                                    if (submenu4.child?.data?.length > 0) {
                                                        return {
                                                            key: submenu4.id,
                                                            icon: submenu4.icon ? React.createElement(submenu4.icon) : null,
                                                            label: submenu4.name,
                                                            children: submenu4.child?.data.map((submenu5: any, key2: number) => {
                                                                if (submenu5.child?.data?.length > 0) {
                                                                    return {
                                                                        key: submenu5.id,
                                                                        icon: submenu5.icon ? React.createElement(submenu5.icon) : null,
                                                                        label: submenu5.name,
                                                                        children: submenu5.child?.data.map((submenu6: any, key2: number) => {
                                                                            if (submenu6.child?.data?.length > 0) {
                                                                                return {
                                                                                    key: submenu6.id,
                                                                                    icon: submenu6.icon ? React.createElement(submenu6.icon) : null,
                                                                                    label: submenu6.name,
                                                                                };
                                                                            } else {
                                                                                return {
                                                                                    key: submenu6.id,
                                                                                    icon: submenu6.icon ? React.createElement(submenu6.icon) : null,
                                                                                    label: submenu6.name,
                                                                                };
                                                                            }
                                                                        }),
                                                                    }
                                                                } else {
                                                                    return {
                                                                        key: submenu5.id,
                                                                        icon: submenu5.icon ? React.createElement(submenu5.icon) : null,
                                                                        label: submenu5.name,
                                                                    };
                                                                }
                                                            }),
                                                        }
                                                    } else {
                                                        return {
                                                            key: submenu4.id,
                                                            icon: submenu4.icon ? React.createElement(submenu4.icon) : null,
                                                            label: submenu4.name,
                                                        };
                                                    }
                                                }),
                                            }
                                        } else {
                                            return {
                                                key: submenu3.id,
                                                icon: submenu3.icon ? React.createElement(submenu3.icon) : null,
                                                label: submenu3.name,
                                            };
                                        }
                                    }),
                                }
                            } else {
                                return {
                                    key: submenu2.id,
                                    icon: submenu2.icon ? React.createElement(submenu2.icon) : null,
                                    label: submenu2.name,
                                };
                            }
                        }),
                    }
                } else {
                    return {
                        key: submenu1.id,
                        icon: submenu1.icon ? React.createElement(submenu1.icon) : null,
                        label: submenu1.name,
                    };
                }
            }),
        };
    } else {
        return {
            key: item.id,
            icon: item.icon ? React.createElement(item.icon) : null,
            label: item.name,
        };
    }
},
);

const App = ({ children }: any) => {
    const [collapsed, setCollapsed] = useState(false);
    const [state, dispatch]: any = useContext(ThemeContext);
    // const [theme, setTheme] = useState<MenuTheme>('light');
    // const { token: { colorBgContainer, borderRadiusLG } } = theme.useToken();

    console.log("ST: ", state);


    const changeTheme = (value: boolean) => {
        dispatch({
            theme: value ? 'dark' : 'light'
        })
        // setTheme(value ? 'dark' : 'light');
    };

    return (
        <Layout>
            <Header style={{ display: 'flex', justifyContent: "space-between", alignItems: 'center' }}>
                <Col span={12} style={{ justifyContent: "end", alignItems: 'end' }} className='mt-2'>
                    <Image
                        preview={false}
                        width={100}
                        className='mt-5'
                        src="https://demo.klola.app/images/logo-blue.svg"
                    />
                    <Button
                        type="text"
                        icon={collapsed ? <PiArrowLineLeftDuotone size={20} className='text-white mb-1 -ml-4' /> : <PiArrowLineRightDuotone size={20} className='text-white mb-1 -ml-4' />}
                        onClick={() => setCollapsed(!collapsed)}
                        style={{
                            fontSize: '16px',
                            width: 64,
                            height: 64,
                            marginLeft: 10
                        }}
                    />
                </Col>
                <Col style={{ display: 'flex', justifyContent: "end", alignItems: 'end' }} span={12}>
                    <UserProfile />
                    <Button
                        type="text"
                        icon={state?.theme === 'dark' ? <PiSun className='text-white mt-1 ml-5' size={25} /> : <PiMoonStars className='text-white mt-1 ml-5' size={25} />}
                        onClick={() => changeTheme(state?.theme === 'light' ? true : false)}
                        style={{
                            marginBottom: 20
                        }}
                    />
                </Col>
            </Header>
            <Layout>
                {/* <Sider trigger={null} collapsible collapsed={collapsed} width={250} style={{ background: colorBgContainer }}> */}
                <Sider trigger={null} collapsible collapsed={collapsed} width={250}>
                    <div className="demo-logo-vertical" />
                    <Menu
                        theme={state?.theme}
                        mode="inline"
                        // defaultSelectedKeys={['13']}
                        style={{ height: '100%', borderRight: 0 }}
                        items={items2}
                    />
                </Sider>
                <Layout style={{ padding: '0 24px 24px' }}>
                    {/* <Breadcrumb style={{ margin: '16px 0' }}>
                        <Breadcrumb.Item>Home</Breadcrumb.Item>
                        <Breadcrumb.Item>List</Breadcrumb.Item>
                        <Breadcrumb.Item>App</Breadcrumb.Item>
                    </Breadcrumb> */}
                    <Content
                        style={{
                            padding: 0,
                            margin: 0,
                            minHeight: 180,
                            // background: colorBgContainer,
                            // borderRadius: borderRadiusLG,
                        }}
                    >
                        {children}
                    </Content>
                </Layout>
            </Layout>
        </Layout>
    );
};

export default App;