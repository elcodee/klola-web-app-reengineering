import React from 'react';
import { DownOutlined, UserOutlined } from '@ant-design/icons';
import type { MenuProps } from 'antd';
import { Avatar, Dropdown, Space } from 'antd';
import Link from 'next/link';

const items: MenuProps['items'] = [
    {
        label: 'menu 1',
        key: '0',
    },
    {
        label: 'menu 2',
        key: '1',
    },
    {
        label: 'menu 3',
        key: '2',
    },
    {
        type: 'divider',
    },
    {
        label: (
            <Link className='text-red' href="/auth/signin">
                Logout
            </Link>
        ),
        key: '3',
    },
];

const UserProfile: React.FC = () => (
    <Dropdown menu={{ items }} trigger={['click']}>
        <a onClick={(e) => e.preventDefault()} style={{ color: "whitesmoke" }}>
            <Space>
                <Avatar size={40} src="https://klola-v2-storage.oss-accelerate.aliyuncs.com/payroll-v2/uploads/assets/2023/08/21/img_64e288f2c7dfe.png?x-oss-process=image/resize,m_fill,h_150,w_150" icon={<UserOutlined />} />
                Demo Admin
                <DownOutlined />
            </Space>
        </a>
    </Dropdown>
);

export default UserProfile;