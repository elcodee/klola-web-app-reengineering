"use client"

import React from "react";
import { Button, Checkbox, Form, Grid, Image, Input, theme, Typography } from "antd";
import { LockOutlined, MailOutlined } from "@ant-design/icons";
import { PiPassword, PiUser } from "react-icons/pi";
import Link from "next/link";

const { useToken } = theme;
const { useBreakpoint } = Grid;
const { Text, Title } = Typography;

export default function SignIn() {
  const { token } = useToken();
  const screens = useBreakpoint();

  const onFinish = (values: any) => {
    console.log("Received values of form: ", values);
  };

  const styles: any = {
    container: {
      margin: "0 auto",
      padding: screens.md ? `${token.paddingXL}px` : `${token.sizeXXL}px ${token.padding}px`,
      width: "380px"
    },
    footer: {
      marginTop: token.marginLG,
      textAlign: "center",
      width: "100%"
    },
    forgotPassword: {
      float: "right"
    },
    header: {
      marginBottom: token.marginXL
    },
    section: {
      alignItems: "center",
      backgroundColor: token.colorBgContainer,
      display: "flex",
      height: screens.sm ? "100vh" : "auto",
      padding: screens.md ? `${token.sizeXXL}px 0px` : "0px"
    },
    text: {
      color: token.colorTextSecondary
    },
    title: {
      fontSize: screens.md ? token.fontSizeHeading2 : token.fontSizeHeading3
    }
  };

  return (
    <section style={styles.section}>
      <div style={styles.container}>
        <div style={styles.header}>
          <Image
            preview={false}
            width={100}
            className='mt-5'
            src="https://demo.klola.app/images/logo-blue.svg"
          />

          <Title style={styles.title}></Title>
          <Text style={styles.text}>
            Managing people anywhere, anytime
          </Text>
        </div>
        <Form
          name="normal_login"
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
          layout="vertical"
          requiredMark="optional"
        >
          <Form.Item
            name="username"
            initialValue={"DEMOACCOUNT"}
            rules={[
              {
                type: "string",
                required: true,
                message: "Please input your Username or Employee ID!",
              },
            ]}
          >
            <Input
              prefix={<PiUser />}
              placeholder="Username or Employee ID"
            />
          </Form.Item>
          <Form.Item
            name="password"
            initialValue={"DEMOACCOUNT"}
            rules={[
              {
                required: true,
                message: "Please input your Password!",
              },
            ]}
          >
            <Input.Password
              prefix={<PiPassword />}
              type="password"
              placeholder="Password"
            />
          </Form.Item>
          {/* <Form.Item>
            <Form.Item name="remember" valuePropName="checked" noStyle>
              <Checkbox>Remember me</Checkbox>
            </Form.Item>
            <a style={styles?.forgotPassword} href="">
              Forgot password?
            </a>
          </Form.Item> */}
          <Form.Item style={{ marginBottom: "0px" }}>
            <Link href="/">
              <Button block type="primary" className="mt-5" htmlType="submit">
                Sign in
              </Button>
            </Link>
            <div style={styles?.footer}>
              <Text style={styles?.text}>Forgot Password?</Text>{" "}
              <Link href="">Request New</Link>
            </div>
          </Form.Item>
        </Form>
      </div>
    </section>
  );
}