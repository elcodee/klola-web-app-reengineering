import { ArrowUpOutlined } from '@ant-design/icons'
import { Card, Statistic, Typography } from 'antd'
import React from 'react'
const { Title, Text } = Typography;


const TotalDepartement = () => {
    return (
        <>
            <Card hoverable bordered={false} style={{ width: 180 }}>
                <Title level={5}>Total Departemen</Title>
                <Statistic
                    value={37}
                    valueStyle={{ color: '#0054A3' }}
                    // precision={0}
                    // prefix={<ArrowUpOutlined />}
                    // suffix="Pegawai"
                />
            </Card>
        </>
    )
}

export default TotalDepartement