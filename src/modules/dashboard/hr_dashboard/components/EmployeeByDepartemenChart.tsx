import React from 'react'
import ReactEChart from "echarts-for-react";

const EmployeeByDepartemenChart = () => {
  const option = {
    // title: {
    //   text: 'Referer of a Website',
    //   subtext: 'Fake Data',
    //   left: 'center'
    // },
    tooltip: {
      trigger: 'item'
    },
    legend: {
      orient: 'vertical',
      left: 'left'
    },
    toolbox: {
      show: true,
      feature: {
        mark: { show: true },
        // dataView: { show: true, readOnly: false },
        // restore: { show: true },
        saveAsImage: { show: true }
      }
    },
    series: [
      {
        // name: 'Access From',
        type: 'pie',
        radius: '60%',
        center: ['70%', '50%'],
        data: [
          { value: 55, name: 'Asset Management' },
          { value: 1, name: 'BOC' },
          { value: 1, name: 'BOD' },
          { value: 53, name: 'Bussiness Development' },
          { value: 10, name: 'Corporate Communications' },
          { value: 16, name: 'Engineering' },
          { value: 151, name: 'Finance / Accounting' },
          { value: 1, name: 'General Affairs' },
          { value: 99, name: 'Human Resources' },
          { value: 200, name: 'Information Technology' },
          { value: 11, name: 'Legal' },
          { value: 124, name: 'Production' },
          { value: 106, name: 'Risk Management' },
          { value: 213, name: 'Sales & Marketing' },
        ],
        emphasis: {
          itemStyle: {
            shadowBlur: 10,
            shadowOffsetX: 0,
            shadowColor: 'rgba(0, 0, 0, 0.5)'
          }
        }
      }
    ]
  };


  return (
    <ReactEChart option={option} />
  )
}

export default EmployeeByDepartemenChart