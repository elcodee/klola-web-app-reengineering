import React from 'react'
import ReactEChart from "echarts-for-react";

const EmployeeGenderChart = () => {
  const option = {
    tooltip: {
      trigger: 'item',
      formatter: '{a} <br/>{b} : {c} ({d}%)'
    },
    legend: {
      top: 'bottom'
    },
    toolbox: {
      show: true,
      feature: {
        mark: { show: true },
        // dataView: { show: true, readOnly: false },
        saveAsImage: { show: true }
      }
    },
    series: [
      {
        name: 'Gender',
        type: 'pie',
        radius: [5, 50],
        center: ['50%', '50%'],
        roseType: 'area',
        itemStyle: {
          borderRadius: 8
        },
        data: [
          { value: 530, name: 'Laki-Laki' },
          { value: 502, name: 'Perempuan' }
        ]
      }
    ]
  };


  return (
    <ReactEChart option={option} />
  )
}

export default EmployeeGenderChart