import React from 'react'
import ReactEChart from "echarts-for-react";

const EmployeeByEducationsChart = () => {
  const option = {
    tooltip: {
      trigger: 'item'
    },
    legend: {
      top: 'bottom'
    },
    toolbox: {
      show: true,
      feature: {
        mark: { show: true },
        // dataView: { show: true, readOnly: false },
        // restore: { show: true },
        saveAsImage: { show: true }
      }
    },
    series: [
      {
        // name: 'Employees by Education',
        type: 'pie',
        radius: [0, 100],
        center: ['50%', '50%'],
        roseType: 'area',
        itemStyle: {
          borderRadius: 4
        },
        data: [
          { value: 2, name: 'D1' },
          { value: 128, name: 'D2' },
          { value: 124, name: 'D3' },
          { value: 120, name: 'D4' },
          { value: 172, name: 'S1' },
          { value: 122, name: 'S2' },
          { value: 116, name: 'S3' },
          { value: 127, name: 'SMA' },
          { value: 130, name: 'SMK' }
        ]
      }
    ]
  };
  
  


  return (
    <ReactEChart option={option} />
  )
}

export default EmployeeByEducationsChart