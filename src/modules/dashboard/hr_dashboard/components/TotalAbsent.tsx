import { ArrowUpOutlined } from '@ant-design/icons'
import { Card, Statistic, Typography } from 'antd'
import React from 'react'
const { Title, Text } = Typography;


const TotalAbsent = () => {
    return (
        <>
            <Card hoverable bordered={false} style={{ width: 180 }}>
                <Title level={5}>Kehadiran</Title>
                <Statistic
                    value={0}
                    valueStyle={{ color: '#0054A3' }}
                    suffix="%"
                    // precision={0}
                    // prefix={<ArrowUpOutlined />}
                />
            </Card>
        </>
    )
}

export default TotalAbsent