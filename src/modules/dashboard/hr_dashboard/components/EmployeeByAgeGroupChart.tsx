import React from 'react'
import ReactEChart from "echarts-for-react";

const EmployeeByAgeGroupChart = () => {
  const option = {
    tooltip: {
      trigger: 'axis',
      // axisPointer: {
      //   type: 'cross',
      //   crossStyle: {
      //     color: '#999'
      //   }
      // }
    },
    toolbox: {
      feature: {
        // dataView: { show: true, readOnly: false },
        // magicType: { show: true, type: ['line', 'bar'] },
        // restore: { show: true },
        saveAsImage: { show: true }
      }
    },
    legend: {
      data: ['Laki-Laki', 'Perempuan']
    },
    xAxis: [
      {
        type: 'category',
        data: ['Kelompok Umur: 18-25', 'Kelompok Umur: 26-35', 'Kelompok Umur: 36-45', 'Kelompok Umur: 46-55', 'Kelompok Umur: 55+'],
        axisPointer: {
          type: 'shadow'
        }
      }
    ],
    yAxis: [
      {
        type: 'value',
        name: 'Laki-Laki',
        min: 0,
        max: 500,
        interval: 125,
        axisLabel: {
          formatter: '{value}'
        }
      },
      {
        type: 'value',
        name: 'Perempuan',
        min: 0,
        max: 500,
        interval: 125,
        axisLabel: {
          formatter: '{value}'
        }
      }
    ],
    series: [
      {
        name: 'Laki-Laki',
        type: 'bar',
        tooltip: {
          valueFormatter: function (value: any) {
            return value as number + ' Employee';
          }
        },
        data: [23, 182, 135, 110, 49]
      },
      {
        name: 'Perempuan',
        type: 'bar',
        tooltip: {
          valueFormatter: function (value: any) {
            return value as number + ' Employee';
          }
        },
        data: [36, 158, 184, 108, 50]
      }
    ]
  };


  return (
    <ReactEChart option={option} />
  )
}

export default EmployeeByAgeGroupChart