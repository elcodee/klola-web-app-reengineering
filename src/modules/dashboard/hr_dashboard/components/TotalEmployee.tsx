import { ArrowUpOutlined } from '@ant-design/icons'
import { Card, Statistic, Typography } from 'antd'
import React from 'react'
const { Title, Text } = Typography;


const TotalEmployee = () => {
    return (
        <>
            <Card hoverable bordered={false} style={{ width: 180 }}>
                <Title level={5}>Total Pegawai</Title>
                <Statistic
                    value={1037}
                    valueStyle={{ color: '#0054A3' }}
                    // precision={0}
                    // prefix={<ArrowUpOutlined />}
                    // suffix="Pegawai"
                />
            </Card>
        </>
    )
}

export default TotalEmployee