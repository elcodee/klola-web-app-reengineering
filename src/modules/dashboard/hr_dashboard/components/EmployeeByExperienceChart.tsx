import React from 'react'
import ReactEChart from "echarts-for-react";

const EmployeeByExperienceChart = () => {
  const option = {
    tooltip: {
      trigger: 'axis',
      // axisPointer: {
      //   type: 'cross',
      //   crossStyle: {
      //     color: '#999'
      //   }
      // }
    },
    toolbox: {
      feature: {
        // dataView: { show: true, readOnly: false },
        // magicType: { show: true, type: ['line', 'bar'] },
        // restore: { show: true },
        saveAsImage: { show: true }
      }
    },
    legend: {
      data: ['Laki-Laki', 'Perempuan']
    },
    xAxis: [
      {
        type: 'category',
        data: ['Pengalaman Kerja: 0-1 Tahun', 'Pengalaman Kerja: 2-3 Tahun', 'Pengalaman Kerja: 4-5 Tahun', 'Pengalaman Kerja: 6-7 Tahun', 'Pengalaman Kerja: 7+ Tahun'],
        axisPointer: {
          type: 'shadow'
        }
      }
    ],
    yAxis: [
      {
        type: 'value',
        name: 'Laki-Laki',
        min: 0,
        max: 500,
        interval: 125,
        axisLabel: {
          formatter: '{value}'
        }
      },
      {
        type: 'value',
        name: 'Perempuan',
        min: 0,
        max: 500,
        interval: 125,
        axisLabel: {
          formatter: '{value}'
        }
      }
    ],
    series: [
      {
        name: 'Laki-Laki',
        type: 'bar',
        tooltip: {
          valueFormatter: function (value: any) {
            return value as number + ' Employee';
          }
        },
        data: [24, 25, 50, 44, 358]
      },
      {
        name: 'Perempuan',
        type: 'bar',
        tooltip: {
          valueFormatter: function (value: any) {
            return value as number + ' Employee';
          }
        },
        data: [16, 40, 58, 58, 364]
      }
    ]
  };


  return (
    <ReactEChart option={option} />
  )
}

export default EmployeeByExperienceChart