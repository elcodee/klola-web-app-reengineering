"use client";

import { Card, Col, Image, Row, Space, Statistic, Typography } from 'antd'
import React from 'react'
import EmployeeStatusChart from './EmployeeStatusChart';
import EmployeeGenderChart from './EmployeeGenderChart';
import TotalEmployee from './TotalEmployee';
import TotalDepartement from './TotalDepartement';
import TotalAbsent from './TotalAbsent';
import EmployeeAbsentChart from './EmployeeAbsentChart';
import EmployeeByAgeGroupChart from './EmployeeByAgeGroupChart';
import EmployeeByExperienceChart from './EmployeeByExperienceChart';
import EmployeeByDepartemenChart from './EmployeeByDepartemenChart';
import EmployeeByEducationsChart from './EmployeeByEducationsChart';
const { Title, Text } = Typography;
const Index = () => {
    return (
        <>
            <Row justify="space-between" gutter={10} align="middle">
                <Col span={12}>
                    <Row justify="space-between" gutter={[10, 30]} align="middle">
                        <Col span={24}>
                            <Card hoverable loading={false} style={{ height: 250 }}>
                                <div className="block">
                                    <Space>
                                        <Image
                                            width={150}
                                            src="https://klola-v2-storage.oss-accelerate.aliyuncs.com/payroll-v2/uploads/assets/2023/07/12/img_64ad9d067add7.png"
                                        />
                                    </Space>
                                    <Row justify="start" align="middle">
                                        <Col span={24}>
                                            <Space>
                                                <div className="block" style={{ marginLeft: 10 }}>
                                                    <Title level={3}>PT. Klola Indonesia</Title>
                                                    <Row justify="space-between" align="middle">
                                                        <Col className='flex'>
                                                            {/* <PiMapPin size={50} /> */}
                                                            <Text> Pondok Indah Office Tower 2, 17th Fl. Jl. Sultan Iskandar Muda Kav.V-TA Pondok Indah, Kec. Kby. Lama Jakarta Selatan</Text>
                                                        </Col>
                                                    </Row>
                                                    <Row className='mt-2' justify="space-between" align="middle">
                                                        <Col className='flex'>
                                                            {/* <PiPhone size={21} /> */}
                                                            <Text> 02175922910</Text>
                                                        </Col>
                                                    </Row>
                                                </div>
                                            </Space>
                                        </Col>
                                    </Row>
                                </div>
                            </Card>
                        </Col>
                        <Col span={24}>
                            <Row justify="space-between" gutter={10} align="middle">
                                <Col>
                                    <TotalEmployee />
                                </Col>
                                <Col>
                                    <TotalDepartement />
                                </Col>
                                <Col>
                                    <TotalAbsent />
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Col>
                <Col span={6}>
                    <Card hoverable bordered={false} style={{ height: 400 }}>
                        <Title level={4}>Pegawai per Status Pegawai</Title>
                        <EmployeeStatusChart />
                    </Card>
                </Col>
                <Col span={6}>
                    <Card hoverable bordered={false} style={{ height: 400 }}>
                        <Title level={4}>Pegawai per Jenis Kelamin</Title>
                        <EmployeeGenderChart />
                    </Card>
                </Col>
            </Row>
            <Row className='mt-5' justify="space-between" gutter={10} align="middle">
                <Col span={24}>
                    <Row justify="center" gutter={[15, 15]} align="middle">
                        <Col span={24}>
                            <Card hoverable bordered={false} style={{ height: 400 }}>
                                <Title level={4}>Tingkat Kehadiran per Departemen</Title>
                                <EmployeeAbsentChart />
                            </Card>
                        </Col>
                        <Col span={12}>
                            <Card hoverable bordered={false} style={{ height: 350 }}>
                                <Title level={4}>Pegawai per Kelompok Umur</Title>
                                <EmployeeByAgeGroupChart />
                            </Card>
                        </Col>
                        <Col span={12}>
                            <Card hoverable bordered={false} style={{ height: 350 }}>
                                <Title level={4}>Pegawai per Pengalaman Kerja</Title>
                                <EmployeeByExperienceChart />
                            </Card>
                        </Col>
                        <Col span={14}>
                            <Card hoverable bordered={false} style={{ height: 400 }}>
                                <Title level={4}>Jumlah Pegawai per Departemen</Title>
                                <EmployeeByDepartemenChart />
                            </Card>
                        </Col>
                        <Col span={10}>
                            <Card hoverable bordered={false} style={{ height: 400 }}>
                                <Title level={4}>Jumlah Pegawai per Pendidikan</Title>
                                <EmployeeByEducationsChart />
                            </Card>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </>
    )
}

export default Index