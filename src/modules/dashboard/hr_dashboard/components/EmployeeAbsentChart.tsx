import React from 'react'
import ReactEChart from "echarts-for-react";

const EmployeeAbsentChart = () => {
  const option = {
    // title: {
    //   text: 'World Population'
    // },
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        type: 'shadow'
      }
    },
    legend: {},
    toolbox: {
      show: true,
      feature: {
        mark: { show: true }, 
        // dataView: { show: true, readOnly: false },
        saveAsImage: { show: true }
      }
    },
    grid: {
      left: '10%',
      right: '0%',
      bottom: '0%',
      // containLabel: true
    },
    xAxis: {
      type: 'value',
      boundaryGap: [0, 0.05]
    },
    yAxis: {
      type: 'category',
      data: [
        'Asset Management',
        'BOC',
        'BOD',
        'Bussiness Development',
        'Corporate Communitaions',
        'Engineering',
        'Finance / Accounting',
        'General Affairs',
        'Human Resources',
        'Information Technology',
        'Legal',
        'Production',
        'Risk Management',
        'Sales & Marketing',
      ]
    },
    series: [
      {
        name: 'Tingkat Kehadiran',
        type: 'bar',
        data: [55, 65, 26, 13, 9, 14, 90, 49, 57, 43, 37, 87, 93, 61]
      }
    ]
  };


  return (
    <ReactEChart option={option} />
  )
}

export default EmployeeAbsentChart