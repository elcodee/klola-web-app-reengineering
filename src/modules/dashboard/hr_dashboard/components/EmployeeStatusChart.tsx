import React from 'react'
import ReactEChart from "echarts-for-react";

const EmployeeStatusChart= () => {
  const option = {
    tooltip: {
      trigger: 'item',
      formatter: '{a} <br/>{b} : {c} ({d}%)'
    },
    legend: {
      top: 'bottom'
    },
    toolbox: {
      show: true,
      feature: {
        mark: { show: true },
        // dataView: { show: true, readOnly: false },
        saveAsImage: { show: true }
      }
    },
    series: [
      {
        name: 'Employee Status',
        type: 'pie',
        radius: [0, 50],
        center: ['50%', '50%'],
        roseType: 'area',
        itemStyle: {
          borderRadius: 2
        },
        data: [
          { value: 7, name: 'Berhenti' },
          { value: 20, name: 'Kontrak' },
          { value: 1014, name: 'Tetap' },
          { value: 61, name: 'Non Pegawai' }
        ]
      }
    ]
  };


  return (
    <ReactEChart option={option} />
  )
}

export default EmployeeStatusChart